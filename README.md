# projetoAPIdummy

Projeto contendo collections com testes na API dummy.

# **Requisitos**
- [node v12.18.3](https://nodejs.org/pt-br/download/)
- npm v6.14.6

# Instalando dependências
- npm install -g newman

# Executando em linha de comando

``` newman run dummy.postman_collection.json --environment dummy.postman_environment ```

